## Summary
This repository is an experiment to see how effective a GitLab workflow can be in facilitating open research. I am using my own lenr (aka cold fusion) research as the guinea pig.

## I'm totally overwhelmed - what's going on?!

Although this whole webpage is probably very overwhelming if you are a newbie to GitLab, I promise you that it has some really useful and powerful features that will allow us to:

1. Publish articles, data and analysis code
2. Discuss ideas in an open, real-time way with the wider community
3. Receive formal open peer-review on work from a subset of the community

all in one place - for free!

If I were building something from scratch, specifically for open research, things would be more intuitive. I'm using GitLab because it's a platform that was built for developers to collaborate on and review code, so it has everything we need....and many things we don't.

*For those asking "why are you not using GitHub?", I prefer GitHub's user interface, but it does not support writing equations in markdown files - a dealbreaker for science research.*



## Which bits of GitLab should I pay attention to?

Look over at your sidebar and you will see

- Repository - Where the files live

- Issues - Where discussions happen

- Merge requests - Where peer review happens

<img src="https://mattlilley.com/uxkyUqMjDfKa5SUtB67c/UsingGitLab01.png">



## How are you using GitLab for open research?

### Repository

GitLab uses git under the hood to keep track of changes (aka commits) to all files. Using git, you can also keep multiple versions of the whole repository - these are called **branches**. My default branch is called **reviewed-work** - it contains only peer reviewed work. You can however still see my other work by switching branches, e.g. work that's not yet been reviewed (**pending-review**) or work in progress (**wip**). This allows me to publish my work immediately but still receive peer review later.

<img src="https://mattlilley.com/uxkyUqMjDfKa5SUtB67c/UsingGitLab02.png">

### Discussion

Everyone online can see discussions inside the **issues** section of GitLab, but only those with a GitLab account can contribute to and create issues -  it's free to sign-up :-). 

Issues are the place for real time blogs, feedback on proposals, discussion with the wider community etc. 

Issues have some similarity with a traditional web forum, but GitLab gives more control, e.g. an issue can be locked so that only collaborators/reviewers of the project are able to continue discussing. 

<img src="https://mattlilley.com/uxkyUqMjDfKa5SUtB67c/UsingGitLab03.png">



### Peer review

Everyone online can see the peer review process going on inside **merge requests**. However, only those GitLab users who have been invited to collaborate on the project can review a file.

Work cannot be merged into the **reviewed-work** branch unless it has at least 2 approvals. For example:

<img src="https://mattlilley.com/uxkyUqMjDfKa5SUtB67c/UsingGitLab04.png">

The beauty of GitLab merge requests is that you can get really specific about the changes you think need to be made using line by line commenting. 

Just hover over a line in a file inside the `changes` tab to start the process.

<img src="https://mattlilley.com/uxkyUqMjDfKa5SUtB67c/UsingGitLab05.png">

<img src="https://mattlilley.com/uxkyUqMjDfKa5SUtB67c/UsingGitLab06.png">

<img src="https://mattlilley.com/uxkyUqMjDfKa5SUtB67c/UsingGitLab07.png">

When you are finished making comments, you can submit your review and the author gets notified.

<img src="https://mattlilley.com/uxkyUqMjDfKa5SUtB67c/UsingGitLab08.png">

The merge requests feature also enables us to have a conversation about any suggestions being made

<img src="https://mattlilley.com/uxkyUqMjDfKa5SUtB67c/UsingGitLab09.png">

and as changes are made you can keep track of them line by line.

<img src="https://mattlilley.com/uxkyUqMjDfKa5SUtB67c/UsingGitLab10.png">



## How do I become a reviewer?

Such enthusiasm - I love it! 

Right now, this is a manual process. Get in touch with me via info@mattlilley.com and I'll add you to the [open-lenr](https://gitlab.com/open-lenr) GitLab group.

I've got ideas for how to automate the process of deciding who is appropriate to be a reviewer, but these will have to wait.



## How do I give feedback?

Please send me a message at info@mattlilley.com. This is a work in progress so I appreciate all the input I can get. 

Thanks for your time and attention.