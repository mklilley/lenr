# Data-Capture

This folder contains code used to capture the following data
- Pressure
  - 60 atm pressure transducer (arduino)
  - 12 atm pressure transducer (arduino)
- Temperature
  - thermocouple (python)
  - IR sensor (arduino)
- Current
  - current transformer (arduino)
