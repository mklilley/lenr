// Code to calculate AC current from this this split core current transformer 
// https://www.amazon.co.uk/dp/B01EFTMCIE/ref=pe_3187911_189395841_TE_3p_dp_1
// See /data/202002-current-tests#current for more context on how this is used

float current;
void setup() {
   Serial.begin(9600);
   pinMode(A1, INPUT);

}

void loop() {
  int readValue;             //value read from the sensor
  int maxValue = 0;          // store max value here
  int minValue = 1023;       // store min value here
   unsigned long start_time = millis();
   while((millis()-start_time) < 1000) //sample for 1 Sec
   {
       readValue = analogRead(A1);
       // see if you have a new maxValue
       if (readValue > maxValue)
       {
           /*record the maximum sensor value*/
           maxValue = readValue;
       }
       if(readValue < minValue) {
        /*record the minimum sensor value*/
        minValue = readValue;
        }
   }

   current =  (maxValue-minValue) * 5.0 /1024.0 / 2.0 * 5.0;
   Serial.print(start_time);
   Serial.print(",");
   Serial.println(current,3);

   delay(9000);

}
