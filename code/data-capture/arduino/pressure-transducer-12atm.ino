// Code to calculate pressure from this transducer
// https://www.ebay.co.uk/itm/5V-PRESSURE-TRANSDUCER-SENSOR-0-175PSI-0-1-2MPa-OIL-GAS-AIR-WATER-0-5-4-5V-1-4/172362543321
// See /data/20190823-NiMesh-PdBurnish-H#pressure for more context on how this is used

unsigned long time;

void setup() {
    Serial.begin(9600);
}
 
void loop(){
int sensorVal=analogRead(A1);
   
  float sensorValAtm = 120.0;
  float sensorValZero = 50.0;
  float sensorValDiff = sensorValAtm - sensorValZero;
  float pressure_pascal = (((float)sensorVal-sensorValAtm)/sensorValDiff + 1.0)*100000.0;
  float pressure_bar = pressure_pascal/1.0e5;

    time = millis();


    Serial.print(time);
    Serial.print(",");
    Serial.println(round(pressure_pascal));
    
    delay(500);
}
 
