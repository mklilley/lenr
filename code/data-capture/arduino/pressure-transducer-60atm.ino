// Code to calculate pressure from this transducer
// https://www.aliexpress.com/item/4000052995750.html
// See /data/20200228-NiMesh-PdBurnish-D#pressure for more context on how this is used

unsigned long time;

void setup() {
    Serial.begin(9600);
}
 
void loop(){
int sensorVal=analogRead(A1);
   
  float sensorValAtm = 118.0;
  float sensorValZero = 103.0;
  float sensorValDiff = sensorValAtm - sensorValZero;
  float pressure_pascal = (((float)sensorVal-sensorValAtm)/sensorValDiff + 1.0)*100000.0;
  float pressure_bar = pressure_pascal/1.0e5;

    time = millis();


    Serial.print(time);
    Serial.print(",");
    Serial.println(round(pressure_pascal));
    
    delay(500);
}
 
