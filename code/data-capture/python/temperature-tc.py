# Code to calculate temperature from this this K type thermocouple
# https://uk.rs-online.com/web/p/thermocouples/3971236/
# connected to this AdaFruit amplifier
# https://www.adafruit.com/product/269
# and read into a Raspbery Pi using help from
# https://learn.adafruit.com/thermocouple/python-circuitpython#python-computer-wiring-4-3
# and 
# https://learn.adafruit.com/thermocouple/python-circ
# See /data/20190823-NiMesh-PdBurnish-H#temperature for more context on how this is used

import time
import board
import busio
import digitalio
import adafruit_max31855

spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
cs = digitalio.DigitalInOut(board.D5)

max31855 = adafruit_max31855.MAX31855(spi, cs)

while True:
        tempC = max31855.temperature
        tempF = tempC * 9 / 5 + 32
        now = str(round(time.time()))
        print('Temperature: {} C {} F '.format(tempC, tempF))
        time.sleep(5)