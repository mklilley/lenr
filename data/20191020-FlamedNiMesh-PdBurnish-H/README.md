# 20191020-FlamedNiMesh-PdBurnish-H

The data in file `20191020-FlamedNiMesh-PdBurnish-H.csv` is from an experiment taking the Pd burnished Ni mesh from my first lenr experiment and exposing it to a kitchen hob flame before exposing it to a hydrogen atmosphere.

For more context behind this experiment, refer to my [Resurrection experiments blog](https://gitlab.com/mklilley/lenr/-/issues/3#note_245307395).


The data in file `20191020-FlamedNiMesh-PdBurnish-H.csv` consists of 3 columns
- Time (in s)
- Temperature (in C) measured from [this Class 1 K-Type thermocouple](https://uk.rs-online.com/web/p/thermocouples/3971236/)
- Pressure (in Pa) measured from a pressure transducer very similar to [this](https://www.ebay.co.uk/itm/5V-PRESSURE-TRANSDUCER-SENSOR-0-175PSI-0-1-2MPa-OIL-GAS-AIR-WATER-0-5-4-5V-1-4/172362543321) (I don't have access to the original eBay listing)

## Time
The time is [Unix time](https://en.wikipedia.org/wiki/Unix_time)

## Temperature

The thermocouple is an [RS Pro Type K Mineral Insulated Thermocouple](https://uk.rs-online.com/web/p/thermocouples/3971236/) with Plain Pot Seal and Lead. It conforms to IEC 584 standards with class 1 tolerance for optimum accuracy. These sensors or probes have a stainless steel 310 probe sheath and a PFA insulated lead. The mineral insulated flexible probe sheath can be bent and formed to suit a wide range of applications. The thermocouple has a single element insulated hot junction for a reduction in electrical interference and a plain pot seal with a temperature rating of 200°C.

At the temperature ranges being measured, class 1 type K thermocouples have an [error of ±1.5C](https://tmseurope.co.uk/applications/thermocouple-rtd-colour-codes-tolerances).

See my [blog post](https://gitlab.com/mklilley/lenr/issues/1#note_213758789) to understand how I used a Raspberry Pi to convert the electrical signal into a temperature value. *Note that the despite the tone of the blog post, the software installed is correct - I just needed to solder the connections together.*

See [/code/data-capture/python/temperature-tc.py](../../code/data-capture/python/temperature-tc.py) for specific data capture code.

## Pressure

The specifications of a [pressure transducer that's visually similar to mine](https://www.ebay.co.uk/itm/5V-PRESSURE-TRANSDUCER-SENSOR-0-175PSI-0-1-2MPa-OIL-GAS-AIR-WATER-0-5-4-5V-1-4/172362543321) are:

- Working pressure range: 0 - 175 PSI (0 - 1.2 MPa)
- Measuring error: +- 1.5 % FSO
- Temperature range: 0 - 85 deg C
- Temperature range error: +-3.5 % FSO
- Response time: <= 2.0 ms
- Working voltage: 5V DC
- Output voltage: 0.5 - 4.5V DC
- Working current: less than or equal to 10 mA
- Cable length: 170mm
- Sensor length: Approx. 51mm
- Screw dimensions: 13mm diameter, 1/4" BSP (G/DN8)
- Sensor material: carbon steel alloy

Errors in pressure transducers can apparently be quite complicated. See [honeywell](https://sensing.honeywell.com/index.php?ci_id=49908) and [hydraulics and pneumatics](https://www.hydraulicspneumatics.com/200/FPE/Sensors/Article/False/6439/FPE-Sensors) for more information.

The measurement error quoted above of ±1.5% FSO corresponds to ±0.015*1200 = ±18kPa.

See my [blog post](https://gitlab.com/mklilley/lenr/issues/1#note_213760800) to understand the details of how I used an Arduino to convert the electrical signal into a pressure value.

The arduino code I used is (see also [/code/data-capture/arduino/pressure-transducer-12atm.ino](../../code/data-capture/arduino/pressure-transducer-12atm.ino))

```{c}
unsigned long time;

void setup() {
  Serial.begin(9600);
}

void loop(){
  int sensorVal=analogRead(A1);

  // minimum sensor value at full vacuum is 33, so minimum voltage is 33.0*5.0/1024.0
  // sensor value at 1 atm is 102
  // delta sensorVal for calculating gradient in `P = m*sensorVal + c` is 69
  // m = 100000.0/69.0
  // 100000.0 = 100000.0/69.0 * 102.0 + c --> c = 100000.0 * (1-102.0/69.0)

  float pressure_pascal = (((float)sensorVal-102.0)/69.0 + 1.0)*100000.0;
  float pressure_bar = pressure_pascal/1.0e5;

  time = millis();

  Serial.print(time);
  Serial.print(",");
  Serial.println(round(pressure_pascal));

  delay(5000);
}

```
