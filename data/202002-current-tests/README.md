# 202002-current-tests

The data in this folder contains measurements from a [YHDC current transformer](https://www.amazon.co.uk/dp/B01EFTMCIE/ref=pe_3187911_189395841_TE_3p_dp_1) which is designed to measure current from an AC source. Analysis using this data can be found at [202002-current-tests](/code/202002-current-tests/current-stability-analysis.ipynb).

See my [control experiment blog post](https://gitlab.com/mklilley/lenr/issues/2#note_284252505) for more context on why this data was collected.

Each file consists of 2 columns
- Time (in s)
- Current (in A) interpreted from voltage measurements from [this YHDC current transformer](https://www.amazon.co.uk/dp/B01EFTMCIE/ref=pe_3187911_189395841_TE_3p_dp_1)

## Time
The time is [Unix time](https://en.wikipedia.org/wiki/Unix_time)

## Current

The YHDC current transformer is connected in a circuit like [this one](https://learn.openenergymonitor.org/electricity-monitoring/ct-sensors/interface-with-arduino) without the need for a burden resistor because the transformer I have bought (SCT013-005) senses up to 5A and converts this to a voltage up to 1V, see [specs](https://images-na.ssl-images-amazon.com/images/I/710V56M%2BleL._AC_SL1500_.jpg).

The [spec](https://images-na.ssl-images-amazon.com/images/I/710V56M%2BleL._AC_SL1500_.jpg) (also more detailed spec [here](https://statics3.seeedstudio.com/assets/file/bazaar/product/101990028-SCT-013-030-Datasheet.pdf)) says the accuracy is 1% and a more thorough analysis of accuracy done by [openenergymonitor](https://learn.openenergymonitor.org/electricity-monitoring/ct-sensors/yhdc-sct-013-000-ct-sensor-report) also gives errors around that value.

To convert the voltage to current, I took inspiration from [hackster.io](https://www.hackster.io/ingo-lohs/ac-current-sensor-182aff) to obtain the max and min voltage by sampling over a 1000ms period. The peak current was then calculated by  `current = (maxV-minV)/ 2.0 * 5.0`. The factor 5.0 comes from the current sensor mapping 5A to 1V. The reason I need to calculate `maxV-minV` is because my circuit creates an voltage offset to ensure that the Arduino never has a negative voltage input (apparently this can damage it).

The arduino code I used is (see also [/code/data-capture/arduino/current-AC.ino](../../code/data-capture/arduino/current-AC.ino))

```{c}
float current;
void setup() {
   Serial.begin(9600);
   pinMode(A1, INPUT);

}

void loop() {
  int readValue;             //value read from the sensor
  int maxValue = 0;          // store max value here
  int minValue = 1023;
   unsigned long start_time = millis();
   while((millis()-start_time) < 1000) //sample for 1 Sec
   {
       readValue = analogRead(A1);
       // see if you have a new maxValue
       if (readValue > maxValue)
       {
           /*record the maximum sensor value*/
           maxValue = readValue;
       }
       if(readValue < minValue) {
        /*record the minimum sensor value*/
        minValue = readValue;
        }
   }

   current =  (maxValue-minValue) * 5.0 /1024.0 / 2.0 * 5.0;
   Serial.print(start_time);
   Serial.print(",");
   Serial.println(current,3);

   delay(9000);

}
```
