# 20200228-NiMesh-PdBurnish-D

The data in file `20200228-NiMesh-PdBurnish-D.csv` is from a lenr experiment that involved burnishing a nickel mesh with palladium and then exposing it to a deuterium atmosphere.

For more context behind this experiment, refer to [My first deuterium experiments blog](https://gitlab.com/mklilley/lenr/-/issues/4#note_297632479).

The data in file `20200228-NiMesh-PdBurnish-D.csv` consists of 5 columns
- Time (in s)
- Temperature (in C) measured from [this Class 1 K-Type thermocouple](https://uk.rs-online.com/web/p/thermocouples/3971236/)
- Pressure (in Pa) measured from this [high temperature pressure transducer](https://www.aliexpress.com/item/4000052995750.html)
- Ambient temperature (in C) measured using the ambient temperature functionality of this [MLX90614 IR sensor](https://www.amazon.co.uk/gp/product/B01LY6NRU6/)
- Current (in A) interpreted from voltage measurements from [this YHDC current transformer](https://www.amazon.co.uk/dp/B01EFTMCIE/ref=pe_3187911_189395841_TE_3p_dp_1)

## Time
The time is [Unix time](https://en.wikipedia.org/wiki/Unix_time)

## Temperature

The thermocouple is an [RS Pro Type K Mineral Insulated Thermocouple](https://uk.rs-online.com/web/p/thermocouples/3971236/) with Plain Pot Seal and Lead. It conforms to IEC 584 standards with class 1 tolerance for optimum accuracy. These sensors or probes have a stainless steel 310 probe sheath and a PFA insulated lead. The mineral insulated flexible probe sheath can be bent and formed to suit a wide range of applications. The thermocouple has a single element insulated hot junction for a reduction in electrical interference and a plain pot seal with a temperature rating of 200°C.

At the temperature ranges being measured, class 1 type K thermocouples have an [error of ±1.5C](https://tmseurope.co.uk/applications/thermocouple-rtd-colour-codes-tolerances).

See my [blog post](https://gitlab.com/mklilley/lenr/issues/1#note_213758789) to understand how I used a Raspberry Pi to convert the electrical signal into a temperature value. *Note that the despite the tone of the blog post, the software installed is correct - I just needed to solder the connections together.*

See [/code/data-capture/python/temperature-tc.py](../../code/data-capture/python/temperature-tc.py) for specific data capture code.

## Pressure

The key but parts of the specification for the high temperature [pressure transducer](https://www.aliexpress.com/item/4000052995750.html) are:

- Working pressure range: 0 - 6 MPa
- Medium temperature range: -40C - 300C
- Environment temperature range: -40C - 80C
- Measuring error: ± 0.1 % F.S
- Zero temperature drift: ±0.03% F.S/C
- Sensitivity temperature drit: ±0.03% F.S/C
- Overload: 200% F.S
- Long term stability: <0.2% F.S/year

Errors in pressure transducers can apparently be quite complicated. See [honeywell](https://sensing.honeywell.com/index.php?ci_id=49908) and [hydraulics and pneumatics](https://www.hydraulicspneumatics.com/200/FPE/Sensors/Article/False/6439/FPE-Sensors) for more information.

The measurement error quoted above of ±1.5% FSO corresponds to ±0.001*6000 = ±6kPa.

See my [blog post](https://gitlab.com/mklilley/lenr/issues/2#note_251428760) to understand the details of how I used an Arduino to convert the electrical signal into a pressure value.

The arduino code I used is (see also [/code/data-capture/arduino/pressure-transducer-60atm.ino](../../code/data-capture/arduino/pressure-transducer-60atm.ino))

```{c}
unsigned long time;

void setup() {
    Serial.begin(9600);
}

void loop(){
int sensorVal=analogRead(A1);

  float sensorValAtm = 118.0;
  float sensorValZero = 103.0;
  float sensorValDiff = sensorValAtm - sensorValZero;
  float pressure_pascal = (((float)sensorVal-sensorValAtm)/sensorValDiff + 1.0)*100000.0;
  float pressure_bar = pressure_pascal/1.0e5;

    time = millis();


    Serial.print(time);
    Serial.print(",");
    Serial.println(round(pressure_pascal));

    delay(500);
}

```

## Current

The YHDC current transformer is connected in a circuit like [this one](https://learn.openenergymonitor.org/electricity-monitoring/ct-sensors/interface-with-arduino) without the need for a burden resistor because the transformer I have bought (SCT013-005) senses up to 5A and converts this to a voltage up to 1V, see [specs](https://images-na.ssl-images-amazon.com/images/I/710V56M%2BleL._AC_SL1500_.jpg).

The [spec](https://images-na.ssl-images-amazon.com/images/I/710V56M%2BleL._AC_SL1500_.jpg) (also more detailed spec [here](https://statics3.seeedstudio.com/assets/file/bazaar/product/101990028-SCT-013-030-Datasheet.pdf)) says the accuracy is 1% and a more thorough analysis of accuracy done by [openenergymonitor](https://learn.openenergymonitor.org/electricity-monitoring/ct-sensors/yhdc-sct-013-000-ct-sensor-report) also gives errors around that value.

To convert the voltage to current, I took inspiration from [hackster.io](https://www.hackster.io/ingo-lohs/ac-current-sensor-182aff) to obtain the max and min voltage by sampling over a 1000ms period. The peak current was then calculated by  `current = (maxV-minV)/ 2.0 * 5.0`. The factor 5.0 comes from the current sensor mapping 5A to 1V. The reason I need to calculate `maxV-minV` is because my circuit creates an voltage offset to ensure that the Arduino never has a negative voltage input (apparently this can damage it).

The arduino code I used is (see also [/code/data-capture/arduino/current-AC.ino](../../code/data-capture/arduino/current-AC.ino))

```{c}
float current;
void setup() {
   Serial.begin(9600);
   pinMode(A1, INPUT);

}

void loop() {
  int readValue;             //value read from the sensor
  int maxValue = 0;          // store max value here
  int minValue = 1023;
   unsigned long start_time = millis();
   while((millis()-start_time) < 1000) //sample for 1 Sec
   {
       readValue = analogRead(A1);
       // see if you have a new maxValue
       if (readValue > maxValue)
       {
           /*record the maximum sensor value*/
           maxValue = readValue;
       }
       if(readValue < minValue) {
        /*record the minimum sensor value*/
        minValue = readValue;
        }
   }

   current =  (maxValue-minValue) * 5.0 /1024.0 / 2.0 * 5.0;
   Serial.print(start_time);
   Serial.print(",");
   Serial.println(current,3);

   delay(9000);

}
```

## Ambient Temperature

The specifications of this [MLX90614 IR sensor](https://www.amazon.co.uk/gp/product/B01LY6NRU6/) are:
- -40°C to +125°C ambient temperature sensor measurement range
- -70°C to +380°C IR sensor temperature measurement range
- 0.14°C temperature resolution
- ±0.5°C accuracy around room temperatures
- High accuracy of 0.5°C over wide temperature
- 90° Field of view
- 4.5 to 5.5V power
- I2C interface, 0x5A is the fixed 7-bit address

I found a [tutorial online](https://miliohm.com/non-contact-temperature-sensor-mlx90614-arduino-tutorial/) that directs you to install the MLX90614 AdaFruit library. The code to make the sensor work is then (see also [/code/data-capture/arduino/temperature-IR.ino](../../code/data-capture/arduino/temperature-IR.ino)):

```{c}
#include <Wire.h>
#include <Adafruit_MLX90614.h>

Adafruit_MLX90614 mlx = Adafruit_MLX90614();

void setup() {
  Serial.begin(9600);

  Serial.println("Adafruit MLX90614 test");  

  mlx.begin();  
}

void loop() {
  Serial.print("Ambient = "); Serial.print(mlx.readAmbientTempC());
  Serial.print("*C\tObject = "); Serial.print(mlx.readObjectTempC()); Serial.println("*C");
  Serial.print("Ambient = "); Serial.print(mlx.readAmbientTempF());
  Serial.print("*F\tObject = "); Serial.print(mlx.readObjectTempF()); Serial.println("*F");

  Serial.println();
  delay(500);
}
```
The above will get the ambient and also the IR sensor reading - I only use the ambient reading.
